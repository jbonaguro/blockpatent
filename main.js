const {app, BrowserWindow, ipcMain} = require('electron');
const url = require('url');
const path = require('path');
var sha256 = require('js-sha256');
var fs = require('fs');
/*var Web3 = require('web3');

web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));*/

let win;

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', function () {
  win = new BrowserWindow({ width: 700, height: 600, frame: false });

  win.loadURL(url.format ({ 
      pathname: path.join(__dirname, 'index.html'), 
      protocol: 'file:', 
      slashes: true 
   }))
  win.setMenu(null);

  console.log(process.argv);
  if(process.argv.length > 2 && process.argv[2] == '-g') {
    win.isResizable(true);
    win.setSize(win.getSize()[0] + 300, win.getSize()[1]);
    win.isResizable(false);
    win.webContents.openDevTools();
  }
  //console.log(web3.eth.coinbase);

  win.on('closed', function () {
    win = null;
  });
});

ipcMain.on('hashFile', (event, arg) => {
	console.log(arg);
	var file = fs.readFileSync(arg);
	console.log(file);
	var hashValue = sha256(file);
	console.log(hashValue);
	event.sender.send('hashedFile', hashValue);
});