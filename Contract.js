var Web3 = require('web3');
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
exports.web3 = web3;		 	

abi = JSON.parse('[{"constant":true,"inputs":[{"name":"hash",'
	+'"type":"string"}],"name":"getOwner","outputs":[{"name":"",'
	+'"type":"address"}],"payable":false,"stateMutability":"view",'
	+'"type":"function"},{"constant":true,"inputs":[{"name":"","type":'
	+'"address"}],"name":"getNumHashes","outputs":[{"name":"","type":'
	+'"uint256"}],"payable":false,"stateMutability":"view","type":'
	+'"function"},{"constant":false,"inputs":[{"name":"hash","type":'
	+'"string"}],"name":"submit","outputs":[],"payable":false,'
	+'"stateMutability":"nonpayable","type":"function"},{"constant":'
	+'true,"inputs":[{"name":"a","type":"address"},{"name":"index",'
	+'"type":"uint256"}],"name":"getHash","outputs":[{"name":"","type":'
	+'"string"}],"payable":false,"stateMutability":"view","type":'
	+'"function"},{"inputs":[],"payable":false,"stateMutability":'
	+'"nonpayable","type":"constructor"}]');

//BlockPatentContract = new web3.eth.Contract(abi);
//exports.BlockPatentContract = BlockPatentContract;

contractInstance = new web3.eth.Contract(abi,
		'0xd26e3d4de35a4275c1f5851abeb2d7e41554f6b3')
/*BlockPatentContract
	.at('0xac62c31624da95d23ac5ad04aebf1a99be55ac11');*/
exports.contractInstance = contractInstance;

function submit(hash) {
	/*contractInstance.submit(hash, {from: web3.eth.accounts[0]},
			function() {});*/
	contractInstance.methods.submit(hash)
			.call({from: '0xAc62C31624dA95d23AC5aD04AeBF1A99be55ac11',
				   gas: 3000000});
}

function getOwner(hash) {	
	/*contractInstance.getOwner(hash, {from: web3.eth.accounts[0]},
			function() {});*/
	return contractInstance.methods.getOwner(hash)
			.call({from: '0xAc62C31624dA95d23AC5aD04AeBF1A99be55ac11',
				   gas: 3000000});
}

function getNumHashes(address) {
	/*contractInstance.getNumHashes(address, {from: web3.eth.accounts[0]},
			function() {});*/
	return contractInstance.methods.getNumHashes(address)
			.call({from: '0xAc62C31624dA95d23AC5aD04AeBF1A99be55ac11',
				   gas: 3000000});
}

function getHash(address, num) {
	/*contractInstance.getHash(address, hash,	
		{from: web3.eth.accounts[0]}, function() {});*/
	return contractInstance.methods.getHash(address, num)
			.call({from: '0xAc62C31624dA95d23AC5aD04AeBF1A99be55ac11',
				   gas: 3000000});
}

exports.submit = submit;
exports.getOwner = getOwner;
exports.getNumHashes = getNumHashes;
exports.getHash = getHash;