var Web3 = require('web3');
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
exports.web3 = web3;		 	

abi = JSON.parse('[
	{
		"constant": false,
		"inputs": [
			{
				"name": "verificationString",
				"type": "uint256"
			}
		],
		"name": "verify",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "a",
				"type": "address"
			}
		],
		"name": "getNumPatents",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "a",
				"type": "address"
			},
			{
				"name": "index",
				"type": "uint256"
			}
		],
		"name": "getPatent",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "hash",
				"type": "uint256"
			}
		],
		"name": "getOwner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "hash",
				"type": "uint256"
			}
		],
		"name": "submit",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "hash",
				"type": "uint256"
			}
		],
		"name": "Submit",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "verificationString",
				"type": "uint256"
			}
		],
		"name": "Verify",
		"type": "event"
	}
]');

//BlockPatentContract = new web3.eth.Contract(abi);
//exports.BlockPatentContract = BlockPatentContract;

contractInstance = new web3.eth.Contract(abi,
		'')
/*BlockPatentContract
	.at('0xac62c31624da95d23ac5ad04aebf1a99be55ac11');*/
exports.contractInstance = contractInstance;

function submit(hash) {
	/*contractInstance.submit(hash, {from: web3.eth.accounts[0]},
			function() {});*/
	contractInstance.methods.submit(hash)
			.call({from: web3.eth.accounts[0],
				   gas: 3000000});
}

function getOwner(hash) {	
	/*contractInstance.getOwner(hash, {from: web3.eth.accounts[0]},
			function() {});*/
	return contractInstance.methods.getOwner(hash)
			.call({from: web3.eth.accounts[0],
				   gas: 3000000});
}

function getNumHashes(address) {
	/*contractInstance.getNumHashes(address, {from: web3.eth.accounts[0]},
			function() {});*/
	return contractInstance.methods.getNumHashes(address)
			.call({from: web3.eth.accounts[0],
				   gas: 3000000});
}

function getHash(address, num) {
	/*contractInstance.getHash(address, hash,	
		{from: web3.eth.accounts[0]}, function() {});*/
	return contractInstance.methods.getHash(address, num)
			.call({from: web3.eth.accounts[0],
				   gas: 3000000});
}

exports.submit = submit;
exports.getOwner = getOwner;
exports.getNumHashes = getNumHashes;
exports.getHash = getHash;