var app = angular.module('BlockPatent', []);

/*var Tab = function(name, content) {
   this.className = '';
   this.name = name;
   this.index = Tab.count++;
}
Tab.count = 0;*/

app.controller('BPController', function($scope) {

	_this = this;

	if (typeof web3 === 'undefined') {
		document.body.innerHTML = '<p style="text-align:center;">Please Enable MetaMask!</p>';
		return;
	}

	$('#vers').text(""+web3.version.api);
	web3.eth.getAccounts((err, a) => {
		$('#acct').text(a[0]);

		_this.account = a[0];

		web3.eth.getBalance(a[0], (err, balance) => {
	    	$('#bal').text(balance);
		});


		c = web3.eth.contract(abi).at('0xba023b835744b272c767e6be1ce7f8343358798f');
	
		$scope.numPatents = 0;
	
		function getMyPatents() {
	
			c.getNumPatents(a[0], (err, res) => {
				var numPatents = res['c'][0];
				console.log(numPatents);
	
				$scope.$apply(() => {
					$scope.patents = [];
					for (var i = 0; i < numPatents; i++) {
						c.getPatent(a[0], i, (err, res) => {
							$scope.$apply(() => {
								var patent = res['c'];
								console.log(patent);
	
								$scope.patents.push(res['c'][0]);
							});
						});
					}
					console.log($scope.patents);
				});
			});
		}
	
		getMyPatents();
	
		console.log($scope.numPatents);
	console.log($scope.patents);
	});
});

/*app.controller('TabController', function($scope) {
	$scope.tabs = [new Tab('Create Account'),
        new Tab('Create BlockPatent')];
	$scope.tabClick = function(i) {
		for(var j = 0; j < $scope.tabs.length; j++) {
			if (i == j) {
				$scope.tabs[j].className = 'selected';
			} else {
				$scope.tabs[j].className = '';
			}
		}
	}

	$scope.isSelected = function(i) {
		if (i == -1) {
			for (var j = 0; j < $scope.tabs.length; j++) {
				if ($scope.tabs[j].className == 'selected') {
					return false;
				}
			}
			return true;
		} else {
			if($scope.tabs[i].className == 'selected') {
				return true;
			}
			return false;
		}
	}
});

app.controller('SubmitController', function($scope) {
	$scope.fileName = 'No file chosen';
	$scope.filePath = '';
	$scope.hashValue = '';

	$scope.hashFile = function() {
		ipcRenderer.send('hashFile', $scope.filePath);
	}

	ipcRenderer.on('hashedFile', (event, arg) => {
		console.log(arg);
		$scope.$apply(function() {
			$scope.hashValue = arg;
		});
	});

	$scope.openDialog = function() {
		dialog.showOpenDialog({multiSelections: false},
				function(fileNames) {
			console.log(fileNames);
			$scope.$apply(function() {
				$scope.filePath = fileNames[0];
				$scope.fileName = path.parse($scope.filePath).base;
			});
		})
	}
});
*/