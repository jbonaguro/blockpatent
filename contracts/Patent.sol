pragma solidity ^0.4.11;

contract BlockPatent {
	mapping(address => string[]) hashes;
	mapping(string => address) owners;

	function BlockPatent() public {
	}

	/* Stores the hash on the blockchain for that address
	*/
	function submit(string hash) public {
		//Hash not already uploaded -- collisions unlikely
		require(owners[hash] == 0);

		owners[hash] = msg.sender;
		hashes[msg.sender].push(hash);
	}
	
	/* Gets the owner of a particular hash
	*/
	function getOwner(string hash) public view returns(address) {
        return owners[hash];
	}
	
	/* Gets the number of hashes for an address
	*/
	function getNumHashes(address) public view returns(uint) {
		uint len = hashes[msg.sender].length;
		return len;
	}
	
	/* Gets the hash at index for an address
	*   Requires that i is between 0 and getNumHashes - 1 for that address
	*/
	function getHash(address a, uint index) public view returns(string) {
		require(index >= 0);
		require(index < hashes[a].length);

		return hashes[a][index];
	}
}