const {ipcRenderer, remote} = require('electron');
const {dialog} = require('electron').remote;
var path = require('path');
var contract = require('./Contract.js');
//var Web3 = require('web3');
//web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var app = angular.module('BlockPatent', ['ngSanitize']);

var Tab = function(name, content) {
   this.className = '';
   this.name = name;
   this.index = Tab.count++;
}
Tab.count = 0;

app.controller('BPController', function($scope) {
	//console.log(web3.eth);
	console.log(contract.web3.eth.accounts[0]);
});

app.controller('TabController', function($scope) {
	$scope.tabs = [new Tab('Create Account'),
        new Tab('Create BlockPatent')];
	$scope.tabClick = function(i) {
		for(var j = 0; j < $scope.tabs.length; j++) {
			if (i == j) {
				$scope.tabs[j].className = 'selected';
			} else {
				$scope.tabs[j].className = '';
			}
		}
	}

	$scope.isSelected = function(i) {
		if (i == -1) {
			for (var j = 0; j < $scope.tabs.length; j++) {
				if ($scope.tabs[j].className == 'selected') {
					return false;
				}
			}
			return true;
		} else {
			if($scope.tabs[i].className == 'selected') {
				return true;
			}
			return false;
		}
	}
});

app.controller('SubmitController', function($scope) {
	$scope.fileName = 'No file chosen';
	$scope.filePath = '';
	$scope.hashValue = '';

	$scope.hashFile = function() {
		ipcRenderer.send('hashFile', $scope.filePath);
	}

	ipcRenderer.on('hashedFile', (event, arg) => {
		console.log(arg);
		$scope.$apply(function() {
			$scope.hashValue = arg;
		});
	});

	$scope.openDialog = function() {
		dialog.showOpenDialog({multiSelections: false},
				function(fileNames) {
			console.log(fileNames);
			$scope.$apply(function() {
				$scope.filePath = fileNames[0];
				$scope.fileName = path.parse($scope.filePath).base;
			});
		})
	}
});
