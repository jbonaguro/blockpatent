//$( document ).ready(function() {
var getWeb3 = function(scope) {
  if (typeof web3 !== 'undefined') {
    startApp(scope);
  } else {
    document.body.innerHTML = '<p style="text-align:center;">Please Enable MetaMask!</p>';
    startApp(scope);
  }
};//});

//var contractInstance;

function startApp(scope) {

  console.log(web3.version.api);

  //while(web3.eth.accounts[0] == undefined);

  console.log(web3.eth.accounts[0]);

  $('#vers').text(""+web3.version.api);
  $('#acct').text(""+web3.eth.accounts[0]);
  web3.eth.getAccounts((err, a) => {
    $('#acct').text(a[0]);

    web3.eth.getBalance(a[0], (err, balance) => {
        $('#bal').text(balance);
      });
  });


  var contract = web3.eth.contract(abi);
  scope.contractInstance = contract.at('0xba023b835744b272c767e6be1ce7f8343358798f');
}


function submit(hash) {
  /*contractInstance.submit(hash, {from: web3.eth.accounts[0]},
      function() {});*/
       return new Promise((resolve) => {
      contractInstance.methods.submit(hash)
      .call({from: web3.eth.accounts[0],
           gas: 3000000});
    })
  
}

function getOwner(hash) {
  /*contractInstance.getOwner(hash, {from: web3.eth.accounts[0]},
      function() {});*/
  return contractInstance.methods.getOwner(hash)
      .call({from: web3.eth.accounts[0],
           gas: 3000000});
}

function getNumPatents(address) {
  /*contractInstance.getNumHashes(address, {from: web3.eth.accounts[0]},
      function() {});*/
  console.log(contractInstance);
  return contractInstance.getNumPatents(address, {from: web3.eth.accounts[0], gas: 3000000},
      console.log);
}

function getPatent(address, num) {
  /*contractInstance.getHash(address, hash,
    {from: web3.eth.accounts[0]}, function() {});*/
  var patent;
  contractInstance.getPatent.call(address, num,
      {from: web3.eth.accounts[0], gas: 3000000}, function(err, p) { patent = p; });
  return patent;
}
