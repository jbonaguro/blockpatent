import Vue from 'vue'
import VueRouter from 'vue-router'
//import AppLayout from './theme/layout.vue'
import TestLayout from './theme/testlayout.vue'
import RegisterView from './views/RegisterView.vue'
import LoginView from './views/LoginView.vue'
import MyPostsView from './views/MyPostsView.vue'
import UploadView from './views/UploadView.vue'
import HomeView from './views/HomeView.vue'
import AllPostsView from './views/AllPostsView.vue'
import AccountView from './views/AccountView.vue'
import ItemView from './views/ItemView.vue'
import VerifyView from './views/VerifyView.vue'

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	base: __dirname,
	routes: [
		//{ path: '/', name: 'AppLayout', component: AppLayout },
		{ path: '/test', name: 'TestLayout', component: TestLayout },
		{ path: '/home', name: 'HomeView', component: HomeView },
		{ path: '/register', name: 'RegisterView', component: RegisterView },
		{ path: '/login', name: 'LoginView', component: LoginView },
		{ path: '/myposts', name: 'MyPostsView', component: MyPostsView },
		{ path: '/all', name: 'AllPostsView', component: AllPostsView },
		{ path: '/upload', name: 'UploadView', component: UploadView },
		{ path: '/verify', name: 'VerifyView', component: VerifyView },
		{ path: '/item/:id', name: 'ItemView', component: ItemView, props: true },
		{ path: '/', redirect: '/home' },
		{ path: '/account', name: '/AccountView', component: AccountView }
	]
});

export default router;
