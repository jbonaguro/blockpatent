import axios from 'axios'

// axios.defaults.baseURL = 'http://dev.cowboykillers.io:3005'
const url = 'http://' + location.hostname + ':' + location.port
axios.defaults.baseURL = url

const appService = {
  getFile(id, filename) {
    return new Promise((resolve) => {
      var src = "/uploads/"+id+"/"+filename;

      axios.get(src).then(response => {
        console.log(response.data);
        resolve(response);
      });
    });
  },

  textFile(id, filename) {
    return new Promise((resolve) => {
      var src =  "/uploads/"+id+"/"+filename;

      axios.get(src).then(response => {
        console.log(response.data);
        resolve(response.data);
      });
    });
  },
  loadPosts () {
    return new Promise((resolve) => {
        console.log(url);
      axios.post('/all')
        .then(response => {
          console.log(response.data)
          resolve(response.data)
        })
    })
  },
  loadMyPosts () {
    return new Promise((resolve) => {
      axios.post('/myposts')
        .then(response => {
          console.log(response.data)
          resolve(response.data)
        })
    })
  },
  loadComments (path) {
    return new Promise((resolve) => {
      axios.post(path)
        .then(response => {
          console.log(response.data)
          resolve(response.data)
        })
    })
  },
  pushComment (push) {
       console.log('hit app.service')
       console.log(push)
       return new Promise((resolve, reject) => {
         console.log('inside promise')
         const config = { headers: { 'Content-Type': 'application/json' } };
         console.log(config);
         axios.post(push.path, push, config)
           .then(response => {
             console.log(response.data)
             resolve(response.data)
           }).catch(response => {
             reject(response.status)
           })
       })
  },
  downloadPost (path) {
    return new Promise((resolve) => {
      axios.post(path)
        .then(response => {
          console.log(response.data)
          resolve(response.data)
        })
    })
  },
  register (credentials) {
       console.log('hit app.service')
       console.log(credentials)
       return new Promise((resolve, reject) => {
         console.log('inside promise')
         console.log(credentials)
         const config = { headers: { 'Content-Type': 'application/json' } };
         console.log(config);
         axios.post('/register', credentials, config)
           .then(response => {
             console.log(response.data)
             resolve(response.data)
           }).catch(response => {
             reject(response.status)
           })
       })
  },
  login (credentials) {
       console.log('hit app.service')
       console.log(credentials)
       return new Promise((resolve, reject) => {
         console.log('inside promise')
         console.log(credentials)
         const config = { headers: { 'Content-Type': 'application/json' } };
         console.log(config);
         axios.post('/login', credentials, config)
           .then(response => {
             console.log(response.data)
             resolve(response.data)
           }).catch(response => {
             reject(response.status)
           })
       })
  },
  logout () {
    return new Promise((resolve, reject) => {
      axios.post('/logout')
        .then(response => {
          resolve(response.data)
        }).catch(response => {
          reject(response.status)
        })
    })
  },
}

export default appService
