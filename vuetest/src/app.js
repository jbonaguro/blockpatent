import Vue from "vue";
import store from './vuex/index.js'
import AppLayout from './theme/layout.vue'
import router from './router'
import VueCookies from 'vue-cookies'
import Notifications from 'vue-notification'

Vue.use(VueCookies)
Vue.use(Notifications)

const app = new Vue({
	router: router,
	render: h => h(AppLayout),
	store: store,
});

export { app, router, store };
