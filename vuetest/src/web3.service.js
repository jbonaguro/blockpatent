//Handles contract calls
import axios from 'axios'
import sha256 from 'sha256'

var web3Service = {}

if (typeof web3 !== 'undefined') {
    // startApp(scope);
    //window.alert('MetaMask Detected!')


var contract = web3.eth.contract([
  {
    "constant": false,
    "inputs": [
      {
        "name": "verificationString",
        "type": "uint256"
      }
    ],
    "name": "verify",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "a",
        "type": "address"
      }
    ],
    "name": "getNumPatents",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "a",
        "type": "address"
      },
      {
        "name": "index",
        "type": "uint256"
      }
    ],
    "name": "getPatent",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "hash",
        "type": "uint256"
      }
    ],
    "name": "getOwner",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "hash",
        "type": "uint256"
      }
    ],
    "name": "submit",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "name": "owner",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "hash",
        "type": "uint256"
      }
    ],
    "name": "Submit",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "name": "owner",
        "type": "address"
      },
      {
        "indexed": false,
        "name": "verificationString",
        "type": "uint256"
      }
    ],
    "name": "Verify",
    "type": "event"
  }
]);
var contractInstance = contract.at('0xba023b835744b272c767e6be1ce7f8343358798f');

web3Service = {
  submit (hash) {
     contractInstance.submit(hash.toString(), {from: web3.eth.accounts[0],
         gas: 3000000}, function(result) {
            return result;
         });
  },
  getOwner(hash, callback) {
    contractInstance.getOwner(hash, {from: web3.eth.accounts[0],
      gas: 3000000}, function(err, result) {
        console.log(result);
        return callback(result);
    });
  },
  getNumPatents(address) {
    /*contractInstance.getNumHashes(address, {from: web3.eth.accounts[0]},
        function() {});*/
    console.log(contractInstance);
    return contractInstance.getNumPatents(address, {from: web3.eth.accounts[0], gas: 3000000}, function(result) {
      return result;
    });
  },
  getPatent(address, num) {
    var patent;
    contractInstance.getPatent.call(address, num,
        {from: web3.eth.accounts[0], gas: 3000000}, function(err, p) { patent = p; });
    return patent;
  },
  getAddress() {
    return web3.eth.accounts[0];
  },
  hashFile(id, filename) {
    return new Promise((resolve) => {
      var src =  "/uploads/"+id+"/"+filename;

      axios.get(src).then(response => {
        var hash = sha256(response.data);
        console.log(hash);
        resolve(hash);
      });
    });
  }
};

} else {
  window.alert('Please enable MetaMask!')
}

export default web3Service
