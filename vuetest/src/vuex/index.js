import Vue from 'vue'
import Vuex from 'vuex'
import appService from '../app.service.js'
import postsModule from './posts'
import web3Module from './web3'

Vue.use(Vuex)

const state = {
    isAuthenticated: false
}

const store = new Vuex.Store({
    modules: {
        postsModule,
        web3Module
    },
    state,
    getters: {
        isAuthenticated: state => {
          return state.isAuthenticated
        }
    },
    actions: {
        logout (context) {
          return new Promise((resolve) => {
            appService.logout()
              .then((data) => {
                context.commit('logout', data)

                resolve()
              })
              .catch(() => window.alert('Could not logout!'))
          })
        },
        login (context, credentials) {
            console.log('hit context')
            return new Promise((resolve) => {
              appService.login(credentials)
                .then((data) => {
                  console.log(data)
                  context.commit('login', data)
                  // if (!data.authorize) {
                  //     window.alert('Incorrect login!')
                  // }
                  resolve()
                })
                .catch(() => window.alert('Could not login!'))
            })
        },
        register (context, credentials) {
            console.log('hit context reg')
            return new Promise((resolve) => {
              appService.register(credentials)
                .then((data) => {
                  console.log(data)
                  context.commit('register', data)
                  // if (!data.authorize) {
                  //     window.alert('Already a user!')
                  // }
                  resolve()
                })
                .catch(() => window.alert('Could not register!'))
            })
        },
        reAuth (context) {
            console.log('hit reAuth index')
            context.commit('reAuth')
        },
        unAuth (context) {
            console.log('hit unAuth index')
            context.commit('unAuth')
        }
    },
    mutations: {
        logout (state, data) {
          state.isAuthenticated = false
        },
        confirm (state, data) {
          state.isAuthenticated = false
        },
        login (state, data) {
          state.isAuthenticated = true
        },
        register (state, data) {
          state.isAuthenticated = true
        },
        reAuth (state) {
          state.isAuthenticated = true
        },
        unAuth (state) {
          state.isAuthenticated = false
        }
    }
})

export default store
