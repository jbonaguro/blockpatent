import appService from '../app.service.js'

const state = {
  posts: [],
  myPosts: [],
  currPost: {},
  comments: []
}

const getters = {

  posts: state => {
      return state.posts
  },
  myPosts: state => {
      return state.myPosts
  },
  currPost: state => {
      return state.currPost
  },
  comments: state => {
      return state.comments
  }

}

const actions = {
  updatePosts (context) {
    appService.loadPosts()
        .then((data) => {
          console.log(data);
          context.commit('updatePosts', { posts: data } )
        })
  },
  updateMyPosts (context) {
    appService.loadMyPosts()
        .then((data) => {
          console.log(data);
          context.commit('updateMyPosts', { posts: data } )
        })
  },
   updateMyAccount (context) {
    appService.loadMyPosts()
        .then((data) => {
          console.log(data);
          context.commit('updateMyPosts', { posts: data } )
        })
  },
  // loadCurrPost (context, post) {
  //   console.log(post);
  //   context.commit('loadCurrPost', post )
  // },
  updateComments (context, path) {
    appService.loadComments(path)
        .then((data) => {
          console.log('printing data from server');
          console.log(data);
          context.commit('updateComments', data )
        })
  },
  postComment (context, push) {
    appService.pushComment(push)
        .then((data) => {
          console.log(data);
        })
  },
  fetchFile (context, path) {
    appService.downloadPost(path)
        .then((data) => {
          console.log(data);
        })
  }
}

const mutations = {
  updatePosts (state, data) {
    state.posts = data.posts
  },
  updateMyPosts (state, data) {
    state.myPosts = data.posts
  },
  // loadCurrPost (state, post) {
  //   state.currPost = post
  // },
  updateComments (state, data) {
      console.log('hit updateComments mutation');
      console.log(data.comments);
      console.log(data.post);
      console.log(data.post[0].user);
    state.currPost = data.post[0]
    state.comments = data.comments
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
