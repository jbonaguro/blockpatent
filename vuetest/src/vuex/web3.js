import web3Service from '../web3.service.js';
import sha256 from 'sha256';

const state = {
    push: '',
    data: '',
    hash: '',
    owner: '',
    free: false
}

const getters = {
    push: state => {
        return state.push;
    },
    data: state => {
        return state.data;
    },
    hash: state => {
        return state.hash;
    },
    owner: state => {
        return state.owner;
    },
    free: state => {
        return state.free;
    }
}

const actions = {
    submitPost (context, hash) {
      web3Service.submit(hash)
      console.log('inside actions => submitPost => web3Service.submit')
      context.commit('submitPost')
    },
    changeData(context, data) {
      console.log("Changing file data in vuex");
      context.commit('mutateData', data);
    }
}

const mutations = {
    submitPost (state) {
      state.push = 'wigglytuff'
    },
    mutateData(state, data) {
      state.data = data;
      state.hash = sha256(data);
      web3Service.getOwner("0x"+state.hash, (result) => {
        state.owner = result;
        state.free = isNaN(parseInt(state.owner, 16));
      });
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
