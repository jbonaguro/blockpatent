const path = require('path');

const config = {
	entry: {
		app: path.resolve(__dirname, "../src/client-entry.js")
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
            test: /\.(png|jp(e*)g|svg|gif|mp4|webm|ogg|mp3|wav|flac|aac)$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 10000, // Convert images < 8kb to base64 strings
                    name: '[name].[ext]?[hash]'
                }
            }]
        	}
		]
	},
	output: {
		path: path.resolve(__dirname, "../dist"),
		publicPath: "/",
		path: "/",
		filename: "assets/js/[name].js"
	},
	devtool: 'eval-source-map'
};

module.exports = config;
