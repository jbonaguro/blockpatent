const express = require('express');
const bodyParser= require('body-parser')
const app = express();
const MongoClient = require('mongodb').MongoClient
var ObjectId = require('mongodb').ObjectId;
var database = undefined;
var databaseUrl = 'mongodb://127.0.0.1:27017/block';
const fs = require('fs');
const path = require('path');
var mime = require('mime');
var formidable = require('formidable'),
    http = require('http'),
    util = require('util');
var dateFormat = require('dateformat');
var now = new Date();
var cookieParser = require('cookie-parser');
var session = require('express-session');
var url  = require('url');
var sesh;
var UrlPattern = require('url-pattern');
var uniqid = require('uniqid');
var axios = require('axios');
var filestore = require('session-file-store')(session);
const multer = require('multer');
const upload = multer({ dest: 'testupload/' });

const history = require('connect-history-api-fallback')
// const history = require('vue-history-api-fallback');


app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(express.static(path.join(__dirname, "static")));
app.use(cookieParser());
app.use(session({resave: true,
  saveUninitialized: true,
  store: new filestore({path: './sessions'}),
  secret: 'ABCDEFGH12345678'}));
app.use('/dist', express.static(path.resolve(__dirname, "./dist")));
app.use(bodyParser.urlencoded({extended: true}))

app.use(history())

var jsonParser = bodyParser.json({ type: 'application/json'});
var fileParser = bodyParser.json({ type: 'multipart/form-data'});

const indexHTML = (() => {
	return fs.readFileSync(path.resolve(__dirname,'./index.html'), 'utf-8');
})();



require("./build/dev-server")(app);



MongoClient.connect(databaseUrl, (err, client) => {
  // ... start the server
   if (err) return console.log(err)
  database = client.db('block')
 	const port = process.env.PORT || 3005;
	app.listen(port, () => {
	console.log('listening on ' + port);
});
})

// app.get('assets/js/app.js', (req, res) => {
// 
// });

app.get('*', (req, res) => {
	//console.log('hey')

	res.write(indexHTML);
	res.end();
});

app.post('/logout', (req, res) => {
  //console.log('hey')
  sesh = 0
  console.log('logged out')
  //res.clearCookie()
  req.session.destroy(function(err) {
    //nothing right now
  });
  res.redirect('/');
});
//Checks if user exist and if not Saves a new User to Mongodatabase
app.post('/register', jsonParser, function(req, res) {

   database.collection('users').findOne({
    'name': req.body.name,
    'email': req.body.email,
    'password': req.body.password,
    'address': req.body.address }, function(err, user) {
      // hanlde err..
      if (user) {
        // user exists
       res.redirect('/register')
        // res.redirect('/register')
      } else {
      	//register user
        //auth.authorize = true;
        //JSON.stringify(auth);
        //console.log(auth);
        //sesh = 1
         database.collection('users').save(req.body, (err, result) => {
    		if (err) return console.log(err)
            req.session.regenerate(function(err) {
            if(err) throw err;
            req.session.userId = req.body._id

            req.session.loggedIn = true;
            req.session.email = req.body.email;
            // res.redirect('/');
            console.log('logged in')

            res.cookie('email', req.body.email)
            res.redirect('/myposts')

          });
    		// res.redirect('/')
  		})
      }
   })

})

//Checks if user exist and if not Saves a new User to Mongodatabase
app.post('/login', jsonParser, function(req, res) {
   console.log('entered login')
   console.log(req.cookies)
   console.log(req.body)
   console.log(req.body.email)
   database.collection('users').findOne({
    'email': req.body.email,
    'password': req.body.password }, function(err, user) {
      // hanlde err..
      if (user) {
        // user exists
        req.session.email = user.email
        sesh = 1
        req.session.regenerate(function(err) {
            if(err) throw err;
            req.session.userId = user._id

            req.session.loggedIn = true;
            req.session.email = user.email;
            // res.redirect('/');
            console.log('logged in')

            res.cookie('email', req.body.email)
            res.redirect('/myposts')
          });
        // res.redirect('/')
      } else {
      	//Register DNE
        console.log('DNE')
        res.redirect('/login')

      }
   })

})

app.post('/myposts', (req, res) => {
	//console.log('hey')
  if (req.session.loggedIn == false) {
    console.log('shouldn show')
    res.redirect('login')
  } else {
        console.log('shoul show')

    database.collection('files').find({'id': req.session.userId}).toArray(function(err, results) {
      if (results) {
        res.send(results)
      } else {
        res.send('upload-post')
      }
      // send HTML file populated with quotes here
    })
  }

});

app.post('/searchPosts/*', (req, res) => {
 var pattern = new UrlPattern('/searchPosts(/:id)');
 var url_parts = url.parse(req.url);
 var id = pattern.match(url_parts.pathname);
 var idd = id.id
  if (req.session.loggedIn == false) {
    console.log('shouldn show')
    res.redirect('login')
  } else {
        console.log('shoul show')

    database.collection('files').find({'user': idd}).toArray(function(err, results) {
      if (results) {
        res.send(results)
      } else {
        res.redirect('/')
      }
      // send HTML file populated with quotes here
    })
  }

});

app.post('/all', (req, res) => {
    database.collection('files').find({'type': 'public'}).toArray(function(err, results) {
      res.send(results)
    })
});

//Saves uploaded file to Mongodatabase with a user keyword and name of file
 app.post('/upload-post', (req, res) => {
  if (req.session.loggedIn == false) {
    res.redirect('/login')
  } else {
    database.collection('users').find(ObjectId(req.session.userId)).toArray(function(err, user) {
        if (user) {
        var form = new formidable.IncomingForm();
           //Save files to directory /files
            //form.uploadDir = path.join(__dirname, '/uploads')

            // every time a file has been uploaded successfully,
            // rename it to it's orignal name
            var si = uniqid();
            fs.mkdirSync(path.join(__dirname, '/uploads/'+si));
            form.uploadDir = path.join(__dirname, '/uploads/'+si);

            form.on('file', function(field, file) {
              fs.rename(file.path, path.join(form.uploadDir, '/'+file.name));
            });

            // log any errors that occur
            form.on('error', function(err) {
              console.log('An error has occured: \n' + err);
            });

        req.user = user;

        form.parse(req, function(err, fields, files) {
          // console.log("fields")
          // console.log(fields)
          // console.log('files')
          // console.log(files.upload.name)

          // console.log(upload)

          var item = {
            user: req.user[0].name,
            file: files.upload.name,
            date: dateFormat(),
            id: req.session.userId,
            type: fields.type,
            fileid: si
          }
          JSON.stringify(item);
          console.log(item)
           database.collection('files').save(item, (err, result) => {
            if (err) return console.log(err)
            res.redirect('/all')

          })

           form.on('end', function() {
            //res.redirect('/')


            });
        });
      } else {
        //User is not logged in
         res.redirect('/login')
      }
    })
  }


    return;

})
//Allows user to comment on a post

 app.post('/upload-comment/*', jsonParser, function(req, res) {
 var pattern = new UrlPattern('/upload-comment(/:id)');
 var url_parts = url.parse(req.url);
 var id = pattern.match(url_parts.pathname);
 var idd = id.id
 console.log(req.body)
  if (req.session.loggedIn == false) {
    res.redirect('login')
  } else {
    database.collection('users').find(ObjectId(req.session.userId)).toArray(function(err, user) {
        if (user) {
          var item = {
            user: user[0].name,
            comment: req.body.comment ,
            date: dateFormat(),
            id: idd
          }
          JSON.stringify(item);
          database.collection('comments').save(item, (err, result) => {
            if (err) return console.log(err)
            res.send(item);
          })
      } else {
        //User needs to login
         // res.redirect('/upload-comment')
         res.redirect('login')
      }
    })
  }
    return;
})

//Retrieves file to download based off of id
 app.post('/download/*', (req, res) => {

  var pattern = new UrlPattern('/download(/:id)');
  var url_parts = url.parse(req.url);

 var id = pattern.match(url_parts.pathname);
 var idd = id.id
  if (req.session.loggedIn == false) {
    res.redirect('login')
  } else {
   var file = __dirname + '/uploads/' + idd + '/' + idd;
  //
  //  var filename = path.basename(file);
  // // var mimetype = mime.getType(file);
  //
  //res.set('Content-disposition', 'attachment; filename=' + filename);

  // var filestream = fs.createReadStream(file);
  // filestream.pipe(res);
   // res.set("Content-Disposition", "attachment;filename=" + file);
  res.download(file);
  }

})

//Retrieves comments based off of id
 app.post('/item/*', (req, res) => {
  console.log('in item')
  var pattern = new UrlPattern('/item(/:id)');
  var url_parts = url.parse(req.url);

 var id = pattern.match(url_parts.pathname);
 var idd = id.id
  if (req.session.loggedIn == false) {
    res.redirect('login')
  } else {
    database.collection('files').find(ObjectId(idd)).toArray(function(err, results) {
      if (results) {
          console.log(results)

        database.collection('comments').find({'id': idd}).toArray(function(err, response) {
          if (response) {
            console.log(results)
            console.log(response)

            //res.send(results)
            res.json({
              comments: response,
              post: results
            });
          } else {
            res.json({
              post: results
            });
          }
        })
      } else {
        res.send('/')
      }
    })
  }

});

 //Checks if user exist and if not Saves a new User to Mongodatabase
app.post('/confirm', jsonParser, function(req, res) {
   console.log('entered confirm')
   console.log(req.cookies)
   console.log(req.body)
   var id = req.session.userId
   console.log(req.body.email)
     if (req.session.loggedIn == false) {
      console.log("successfullu")
    res.send('login')
  } else {
    console.log("in else")
    console.log(id)
    database.collection('users').findOneAndUpdate({email: req.body.email}, {
      $set: {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }

  }),
   {
    sort: {_id: -1},
    upsert: true
  }, (err, result) => {
    console.log('before')
    if (err) return res.send(err)
    console.log("heree")
    res.redirect('/myposts')
  }
  }

})
  


//Testing Axios file upload
app.post('/img', upload.single('file'), (req, res) => {
  res.redirect('/');
});
